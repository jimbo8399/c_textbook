#include <stdio.h>
#include <string.h>
#include <math.h>

int htoi(char[]);

int main() {
    
    printf("%d\n",htoi("0xFF"));
    return 0;
}


int htoi(char hexNum[]){
    
    int i, intNum, n, p, stop;
    stop = intNum = n = p = 0;
    
    if (strlen(hexNum) > 2) {
        if ( hexNum[0] == '0' && (hexNum[1] == 'x' || hexNum[1] == 'X')) {
            stop = 2;
        }
    }
    
    for ( i = strlen(hexNum)-1; i>=stop; i--) {
        if ( hexNum[i] != 0 ) {
            if (hexNum[i]>='0' && hexNum[i]<='9') {
                intNum += (hexNum[i] - '0') * pow(16,p);
            }else if ( hexNum[i]>= 'a' && hexNum[i] <='f' ) {
                intNum += (hexNum[i] - 'a' + 10) * pow(16,p);
            } else if ( hexNum[i] >= 'A' && hexNum[i] <= 'F') {
                intNum += (hexNum[i] - 'A' + 10) * pow(16,p);
            }
            p++;
        }
    }
    return (int)intNum;
}
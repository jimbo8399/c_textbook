#include <stdio.h>

/*count digit occurrences, white spaces, others */

int main() {
    int c, i, nwhite, nother;
    int ndigit[10];
    
    nwhite = nother = 0;
    for (i = 0; i<10; ++i){
        ndigit[i] = 0;
    }
    
    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9') { //determines if the character is a digit
            ++ndigit[c-'0']; // digits have consecutive character values
        }else if (c == ' ' || c == '\n' || c == '\t') {
            ++nwhite;
        }else{
            ++nother;
        }
    }
    
    printf("digits =");
    for (i = 0; i<10; ++i){
        printf(" %d", ndigit[i]);
    }
    printf(", white spaces = %d, other = %d", nwhite, nother);
    return 0;
}
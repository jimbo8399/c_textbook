#include <stdio.h>

int main() {
    
    int c, i,i2;
    int nchars[126];
    
    for (i = 0; i < 126; i++) {
        nchars[i] = 0;
    }
    
    while ((c = getchar()) != EOF) {
        ++nchars[c];
    }
    
    for (i = 32; i < 126; i++) {
        if (nchars[i] != 0){
            printf("%c ", i);
            for ( i2 = 0; i2 < nchars[i]; i2++) {
                putchar('=');
            }
            putchar('\n');
        }
    }
    return 0;
}
#include <stdio.h>

/* horizontal histogram of word length */

int main() {
    int c;
    
    while ((c = getchar()) != EOF) {
        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
            putchar('=');
        }else if (c == ' ' || c == '\t' || c == '\n') {
            putchar('\n');
        }
    }
    return 0;
}
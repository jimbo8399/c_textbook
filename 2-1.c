#include <stdio.h>
#include <limits.h>

int main() {

   printf("The number of bits in a byte %d\n\n", CHAR_BIT);
   
   printf("The minimum value of CHAR = %d\n", CHAR_MIN);
   printf("The maximum value of CHAR = %d\n\n", CHAR_MAX);

   printf("The minimum value of SIGNED CHAR = %d\n", SCHAR_MIN);
   printf("The maximum value of SIGNED CHAR = %d\n\n", SCHAR_MAX);
   
   printf("The minimum value of UNSIGNED CHAR = %d\n", 0);
   printf("The maximum value of UNSIGNED CHAR = %d\n\n", UCHAR_MAX);

   printf("The minimum value of SHORT INT = %d\n", SHRT_MIN);
   printf("The maximum value of SHORT INT = %d\n\n", SHRT_MAX);
   
   printf("The minimum value of UNSIGNED SHORT INT = %d\n", 0);
   printf("The maximum value of UNSIGNED SHORT INT = %d\n\n", USHRT_MAX);

   printf("The minimum value of INT = %d\n", INT_MIN);
   printf("The maximum value of INT = %d\n\n", INT_MAX);
   
   printf("The minimum value of UNSIGNED INT = %d\n", 0);
   printf("The maximum value of UNSIGNED INT = %u\n\n", UINT_MAX);
   
   printf("The minimum value of LONG = %ld\n", LONG_MIN);
   printf("The maximum value of LONG = %ld\n\n", LONG_MAX);
   
   printf("The minimum value of UNSIGNED LONG = %d\n", 0);
   printf("The maximum value of UNSIGNED LONG = %lu\n\n", ULONG_MAX);
  
   return(0);
}
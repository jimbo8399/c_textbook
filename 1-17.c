#include <stdio.h>
#define MAXLINE 1000

int getline(char line[], int maxline);
void copy(char to[], char from[], int n);

/*print lines longer than 80 chars*/

int main() {
    int n; /* position of last added character */
    int len;
    int found;
    char line[MAXLINE];
    char newline[MAXLINE];
    
    found = 0;
    n = 0;
    while ((len = getline(line,MAXLINE)) > 0) {
        if (len-1 > 80) {
            found = 1;
            copy(newline, line, n);
            n = len;
        }
    }
    if (found > 0) /* there was a line */ {
        printf("%s", newline);
    }
    return 0;
}

/* getline: read a line into s, return length */

int getline(char line[], int maxline) {
    int c, i;
    
    for (i = 0; i < maxline-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */

void copy(char to[], char from[], int n) {
    
    int i;
    
    i = 0;
    while ((to[n + i] = from[i]) != '\0') {
        ++i;
    }
}
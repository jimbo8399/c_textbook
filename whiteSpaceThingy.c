#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX 100

int readline(char line[], int max) {
    if (fgets(line,max,stdin) == NULL)
        return 0;
    else
        return strlen(line);
}

main() {
    char line[MAX];
    int whitespace = 0;
    while( readline(line,MAX) > 0) {
        for (int i = 0; i< strlen(line); i++) {
            if ( ispunct(line[i])) {
                continue;
            } else if ( isspace(line[i]) && whitespace == 0 ) {
                putchar(' ');
                whitespace++;
            }else if (!isspace(line[i])) {
                putchar(line[i]);
                whitespace = 0;
            }
        }
        putchar('\n');
    }
    return 0;
}

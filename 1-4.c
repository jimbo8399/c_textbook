#include <stdio.h>

/* print Fahrenheit-Celcius table
	for celsius = 0, 20, ..., 300 */

int main(){
	int fahr, celsius;
	int lower,upper,step;
	
	lower = 0;
	upper = 300;
	step = 20;
	
	celsius = lower;
	printf("Temperaure conversion program\n");
	printf("Celcius\\Fahrenheit\n");
	while (celsius <= upper){
		fahr = ((celsius*9)/5) + 32;
		printf("%d\t %d\n", celsius, fahr);
		celsius += step;
	}
	return 0;
}
#include <stdio.h>

#define IN 1
#define OUT 0

int main() {
    int c;
    int state = OUT;
    
    while( (c = getchar()) != EOF){
        if ( c == ' ' || c == '\t'){
            state = OUT;
            c = '\n';
            putchar(c);
        }else{
            putchar(c);
        }
    }
    return 0;
}
#include <stdio.h>

/* print Frenheir-Celcius table
	for celsius = 0, 20, ..., 300 */

main(){
	int fahr, celsius;
	int lower,upper,step;
	
	lower = 0;
	upper = 300;
	step = 20;
	
	celsius = upper;
	printf("Temperature conversion program\n");
	printf("Fahrenheit\\Celcius\n");
	while (celsius >= lower){
		fahr = celsius*9/5 +32;
		printf("%d\t%d\n", fahr, celsius);
		celsius -= step;
	}
	return 0;
}
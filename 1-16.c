#include <stdio.h>
#define MAXLINE 1000

int getline(char line[], int maxline);
void copy(char to[], char from[]);

/*print longest input line*/

int main() {
    int len;
    char line[MAXLINE];
    char longest[MAXLINE];
    
    while ((len = getline(line,MAXLINE)) > 0) {
        printf("%s%d\n", line, len-1);
    }
    return 0;
}

/* getline: read a line into s, return length */

int getline(char line[], int maxline) {
    int c, i;
    
    for (i = 0; i < maxline-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#define MAX 100

int readline(char line[], int max) {
    if (fgets(line,max,stdin) == NULL)
        return 0;
    else
        return strlen(line);
}

main() {
    int c;
    int pos = 0;
    char line[MAX];
    char word[MAX] = "";
    int i;
    while ((readline(line, MAX)) > 0) {
        for (i = 0; i< strlen(line); i++){
            if (isspace(line[i])||ispunct(line[i])){
                putchar('\n');
            else
                putchar(line[i]);
        }
    }
    return 0;
}           

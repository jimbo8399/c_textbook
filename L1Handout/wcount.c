#include <ctype.h>
#include <stdio.h>
#include <string.h>

main() {
    int c;
    int counter = 0;
    while( (c = getchar()) != EOF) {
        if (isspace(c)) {
            counter++;
        }
    }
    printf("%d\n", counter);
}

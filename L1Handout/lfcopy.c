#include <stdio.h>
#include <string.h>
#define MAX 100
/* readline: read a line from std input, return its length or 0 */

int readline(char line[], int max) {
    if (fgets(line,max,stdin) == NULL)
        return 0;
    else
        return strlen(line);
}

/* writeline: write line to std output, return number of chars written */

int writeline(const char line[]) {
    fputs(line, stdout);
    return strlen(line);
}

main() {
    char line[MAX];
    while( readline(line,MAX) > 0) {
        writeline(line);
    }
    return 0;
}

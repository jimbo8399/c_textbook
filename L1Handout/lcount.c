#include <stdio.h>
#include <string.h>
#define MAX 100
/* readline: read a line from standard input, retrun its length or 0 */
int readline(char line[], int max) {
    if( fgets(line, max, stdin) == NULL)
        return 0;
    else
        return strlen(line);
}

main() {
    char line[MAX];
    int counter = 0;
    while( readline(line,MAX) != 0 ) {
        counter++;
    }
    printf("%d\n",counter);
    return 0;
}

#include <stdio.h>

/* print EOF */

int main(){
	if (EOF == 1){
		printf("getchar() != EOF is %d", 1);
	}else if( EOF == 0){
		printf("getchar() != EOF is %d", 0);
	}else{
		printf("EOF is %d\n", EOF);
	}
	return 0;
}

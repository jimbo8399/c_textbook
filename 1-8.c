#include <stdio.h>

/* count lines in input */
int main(){
	int c, num;
	
	num = 0;
	while((c = getchar()) != EOF){
		if (c ==' ' || c == '\t' || c == '\n'){
			++num;
		}
	}
	printf("\n%d\n",num);
	return 0;
}

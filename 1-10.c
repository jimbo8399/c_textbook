#include <stdio.h>

int main() {
    
    int c;
    
    while ( (c = getchar()) != EOF ) {
        if ( c == '\t') {
            c = '\\';
            putchar(c);
            c = 't';
            putchar(c);
        }else if ( c == '\b'){
            c = '\\';
            putchar(c);
            c = 'b';
            putchar(c);
        }else if ( c == '\\'){
            c = '\\';
            putchar(c);
            putchar(c);
        }else{
            putchar(c);
        }
    }
    return 0;
}
#include <stdio.h>

int main(){
    
    int c = getchar();
    int blank = 0;
    
    while( c != EOF ){
        if( c != ' '){
            blank = 0;
            putchar(c);
        }else if( blank == 0 ){
            ++blank;
            putchar(c);
        }
        c = getchar();
    }
    return 0;
}
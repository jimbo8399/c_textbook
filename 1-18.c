#include <stdio.h>
#define MAXLINE 1000
#define TRUE 1

int getline(char line[], int maxline);

int main() {
    char text[MAXLINE];
    char line[MAXLINE];
    int len;
    
    while((len = getline(line,MAXLINE)) > 0) {
        if (line[0] != '\n'){
            printf("%s\n", line);
        }
    }
    return 0;
}

int getline(char line[MAXLINE], int maxline) {
    
    int c, i;
    
    c = getchar();
    i = 0;
    while (i < maxline-1 && c != EOF && c != '\n') {
        if ( c != '\t'){
            line[i] = c;
            c = getchar();
            i++;
        }else {
            c = getchar();
        }
    }
    while (line[i-1] == ' ') {
        line[i-1] = '\n';
        line[i] = ' ';
        --i;
    }
    if (line[0] != '\n') {
        line[i] = '\0';
    }
    return i;
}

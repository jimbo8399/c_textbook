#include <stdio.h>
#include <string.h>
#define MAXLINE 1000
#define TAB 10

int getline(char line[], int maxline);

int main() {
    
    char line[MAXLINE];
    char newline[MAXLINE];
    int tab, len;
    
    while((len = getline(line, MAXLINE)) > 0) {
        int i, p, ind;
        int column = tab = i = p = 0;
        for ( i = 0; i < len; i++ ) {
            if (line[i] != ' ' && tab != TAB) {
                
                ind = (column*TAB) + tab;
                newline[ind] = line[i];
                ++tab;
            }
            else if ( (line[i] == ' ' || line[i] == '\t') && tab != 0) {
                ++column;
                for (int p = TAB - tab ; p > 0; p--) {
                    ind = column*TAB - p;
                    newline[ind] = ' ';
                }
                tab = 0;
            }
            else if (line[i] == '\0') {
                ind = column*TAB + tab;
                newline[ind] = '\0';
            }
        }
        printf("%s\n", newline);
        memset( line, 0, MAXLINE );
        memset( newline, 0, MAXLINE );
    }
    return 0;
}

int getline(char line[], int maxline) {
    int c, n;
    
    for( n = 0; n < maxline-1 && (c = getchar()) != EOF && c != '\n'; n++ ) {
        line[n] = c;
    }
    line[n] = '\0';
    return n;
}
#include <stdio.h>

/* print Frenheir-Celcius table
	for fahr = 0, 20, ..., 300 ; floating-point version */
	
int ftoc(float);

int main(){
	float fahr, celsius;
	int lower,upper,step;
	
	fahr = 0;
	lower = 0;
	upper = 300;
	step = 20;
	
	fahr = lower;
	printf("Temperature conversion program\n");
	printf("Fahrenheit\\Celcius\n");
	while (fahr <= upper){
		//printf("%f \n", ftoc(fahr));
		//printf("%3.0f \t %6.1f\n", fahr, ftoc(fahr));
		//not working...
		celsius = ftoc(fahr);
		printf("%3.0f \t %6.1f\n", fahr, celsius);
		fahr += step;
	}
	return 0;
}

int ftoc(float fahr1){
    return (5.0/9.0)*(fahr1-32.0);
}
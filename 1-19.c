#include <stdio.h>
#define MAXLINE 1000

int getline(char line[], int maxline);

int main() {
    int len, i;
    char line[MAXLINE];
    char newline[MAXLINE];
    
    while ((len = getline(line,MAXLINE)) > 0) {
        int n = 0;
        for( i = len-2; i >= 0; i--){
            newline[n] = line[i];
            ++n;
        }
        newline[n] = '\0';
        printf("%s\n", newline);
    }
}

int getline(char line[], int maxline) {
    int c, i;
    
    for( i = 0; i < maxline-1 && (c = getchar()) != EOF && c != '\n'; i++ ) {
        line[i] = c;
    }
    line[i] = '\0';
    return ++i;
}